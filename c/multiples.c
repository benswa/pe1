/*
Project Euler: Problem 1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these
multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.
*/
#include <stdio.h>

#define True 1
#define False 0

int multiple_of_five(int number){
  /*
  This function is used to detect if the number if a multiple of 5. This is done by using modulo arithmetic.
  */
  if((number % 5) == 0){
    return True;
  }
  else{
    return False;
  }
}


int multiple_of_three(int number){
  /*
  This function is used to detect if the number if a multiple of 3. This is done by using modulo arithmetic.
  */
  if((number % 3) == 0){
    return True;
  }
  else{
    return False;
  }
}


int main(){
  int sum_of_multiples = 0;
  int value;
  for(value = 0; value < 1000; value++){
    if((multiple_of_five(value) == True) || (multiple_of_three(value) == True)){
      sum_of_multiples += value;
    }
  }
  printf("For values below 1000. The sum of those values divisible by 3 or 5 is %i.\n", sum_of_multiples);
  
  return 0;
}
