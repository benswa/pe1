"""
Project Euler: Problem 1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these
multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.
"""

__author__ = 'Ben'


def multiple_of_five(number):
    """
    This function is used to detect if the number if a multiple of 5. This is done by using modulo arithmetic.
    """
    if (number % 5) is 0:
        return True
    else:
        return False


def multiple_of_three(number):
    """
    This function is used to detect if the number if a multiple of 3. This is done by using modulo arithmetic.
    """
    if (number % 3) is 0:
        return True
    else:
        return False


if __name__ == '__main__':
    sum_of_multiples = 0
    for value in range(0, 1000):
        if (multiple_of_five(value) is True) or (multiple_of_three(value) is True):
            sum_of_multiples += value
    print("For values below 1000. The sum of those values divisible by 3 or 5 is {0}.".format(sum_of_multiples))